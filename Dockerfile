FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code
WORKDIR /app/code

# renovate: datasource=github-releases depName=knadh/listmonk versioning=semver extractVersion=^v(?<version>.+)$
ARG LISTMONK_VERSION=4.1.0

RUN curl -L https://github.com/knadh/listmonk/releases/download/v${LISTMONK_VERSION}/listmonk_${LISTMONK_VERSION}_linux_amd64.tar.gz | tar zxvf -

# extract a copy of this versions templates to get started if needed
RUN mkdir -p /app/pkg/static.template && cd /app/pkg/static.template && \
    curl -L https://github.com/knadh/listmonk/archive/refs/tags/v${LISTMONK_VERSION}.tar.gz | tar zxvf  - listmonk-${LISTMONK_VERSION}/static/ --strip-components=2

COPY env.sh.template start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]

