[0.1.0]
* Initial version

[0.2.0]
* Fixes to manifest
* Fix image upload

[0.3.0]
* Fix password reset [issue](https://forum.cloudron.io/topic/7287/after-updating-password-no-credentials-needed-to-login)

[1.0.0]
* Update logo url to relative url
* Add tests

[1.1.0]
* Update Listmonk to 2.2.0
* [Full changelog](https://github.com/knadh/listmonk/releases/tag/v2.2.0)
* The biggest new addition is the new transactional messaging feature. See [docs](https://listmonk.app/docs/apis/transactional/)
  * Define transactional templates (new template type: transactional)
  * API to fire arbitrary transactional messages using predefined templates via any messenger.
* A new Test connection feature on the SMTP settings UI to quickly test SMTP settings by sending a test e-mail. I don't know how I missed this for so long!
* One-click SMTP configuration for popular providers.

[1.1.1]
* Fix packaging error to actually update Listmonk to 2.2.1

[1.1.2]
* allow single quotes in mail display name
* scan for bounces every 3 minutes

[1.1.2-1]
* Keep a copy of upstream templates for the current version

[1.1.3]
* Customizable i18n directory

[1.2.0]
* Update Listmonk to 2.3.0
* [Full changelog](https://github.com/knadh/listmonk/releases/tag/v2.3.0)
* Public campaign archive.
* Subscriber preferences self-management.
* Database maintenance tools.
* Public list descriptions.
* New languages: Vietnamese (@mannm123), Chinese (@jinrenjie), Catalan (@davidesteve), Taiwanese (@ssangyongsports)
* Bug fixes and improvements.

[1.3.0]
* Use base image 4.0.0

[1.4.0]
* Update Listmonk to 2.4.0
* [Full changelog](https://github.com/knadh/listmonk/releases/tag/v2.4.0)
* hCaptcha integration on public subscription pages. If you use custom static static templates, make sure to incorporate the changes from the repository.
* Support for SVG files in media uploads.
* Support for file attachments in the /api/tx transactional API.
* Support for multiple subscribers in the /api/tx transactional API (subscriber_emails[], subscriber_ids[]).

[1.5.0]
* Update Listmonk to 2.5.0
* [Full changelog](https://github.com/knadh/listmonk/releases/tag/v2.5.0)
* The major new feature in this release is support for arbitrary file attachments. The media "gallery" UI/UX has changed to accept arbitrary file uploads which can be attached to campaigns.
* "Record opt-in IP" option in Settings -> Privacy that optionally records the IP from which a subscriber completes a double opt-in. This is a compliance requirement in certain jurisdictions.
* A new table on the subscriber editing popup that shows subscription details for all lists (including the optional opt-in IP)
* New API /api/about that returns generally useful system and installation info.
* Better error notification. Async errors that were logged in Settings -> Logs are now streamed to the UI and displayed as toasts.
* New bounce types and support for variable actions per type.

[1.5.1]
* Update Listmonk to 2.5.1
* [Full changelog](https://github.com/knadh/listmonk/releases/tag/v2.5.1)
* The major new feature in this release is support for arbitrary file attachments. The media "gallery" UI/UX has changed to accept arbitrary file uploads which can be attached to campaigns.
* "Record opt-in IP" option in Settings -> Privacy that optionally records the IP from which a subscriber completes a double opt-in. This is a compliance requirement in certain jurisdictions.
* A new table on the subscriber editing popup that shows subscription details for all lists (including the optional opt-in IP)
* New API /api/about that returns generally useful system and installation info.
* Better error notification. Async errors that were logged in Settings -> Logs are now streamed to the UI and displayed as toasts.

[1.6.0]
* Update base image to 4.2.0

[1.7.0]
* Update Listmonk to 3.0.0
* [Full changelog](https://github.com/knadh/listmonk/releases/tag/v3.0.0)
* The concurrent core or the "engine" is completely re-written. Campaign pausing and cancellations are now near-instant and lossless. Concurrent campaign sending performance has improved significantly. The sent count now accounts for every single message rather than an approximation.
* Major performance improvements for large databases with the new Settings -> Performance -> Cache slow queries option. On installations with millions of subscribers, enabling this speeds up database queries (retrieving lists, subscribers etc.) by several orders of magnitude. On our production database with ~14 million subscribers, the speed up on the lists page was 2000x, from ~20s to ~10ms.
* New frontend dependency and build system refactor, speeding the admin pages (and the development build process).
* Addressed the root cause of mysterious SMTP sending errors that triggered only in certain SMTP environments.
* New fields for filtering lists, campaigns, subscribers in query APIs.
* Major refactor and cleanup of documentation.
* F9 shortcut key for campaign and template previews.
* Many quality-of-life improvements, fixes, and dependency upgrades.

[1.7.1]
* Fix logo url to be full url

[1.8.0]
* Add OpenID integration

[1.9.0]
* Update listmonk to 4.1.0
* [Full Changelog](https://github.com/knadh/listmonk/releases/tag/v4.1.0)
* Multi-user support with granular permissions, user, role, per-list permissions and API token management.
* First-time Super Admin setup UI for fresh installations.
* Significant performance improvements to SQL queries underlying concurrent campaign processing. Performance gains of several orders of magnitude on large installations.
* Styling improvements to UI for better UX including new tabs UI in subscriber modal popup.
* Markdown syntax highlighting.
* Static email template subjects are now scriptable with template syntax.
* Support for CC and BCC in custom email headers.
* Syntax highlighting in HTML form generator.
* Many quality-of-life improvements, fixes, and dependency upgrades.

[1.10.0]
* Add checklist

