#!/bin/bash

set -eu

function update_setting_json() {
    # value is json value type. simple strings have to be quoted
    # positional arguments allows single quotes in value (like email display name)
    echo "INSERT INTO settings(key, value) VALUES(:'v1', :'v2') ON CONFLICT (key) DO UPDATE SET value=:'v2'" | PGPASSWORD=${CLOUDRON_POSTGRESQL_PASSWORD} psql -h ${CLOUDRON_POSTGRESQL_HOST} -p ${CLOUDRON_POSTGRESQL_PORT} -U ${CLOUDRON_POSTGRESQL_USERNAME} -d ${CLOUDRON_POSTGRESQL_DATABASE} -t -v v1="$1" -v v2="$2"
}

mkdir -p /app/data/uploads /run/listmonk /app/data/static/public /app/data/static/email-templates /app/data/i18n

touch /run/listmonk/config.toml # dummy file

if [[ ! -f /app/data/env.sh ]]; then
    echo "==> Installing on first run"
    cp /app/pkg/env.sh.template /app/data/env.sh
    source /app/data/env.sh
    LISTMONK_ADMIN_USER=admin LISTMONK_ADMIN_PASSWORD=changeme /app/code/listmonk --yes --idempotent --install --config /run/listmonk/config.toml

    # one time settings
    update_setting_json app.notify_emails '["admin@cloudron.local"]'
    update_setting_json upload.filesystem.upload_path '"/app/data/uploads"'
    update_setting_json app.check_updates false
    update_setting_json app.logo_url \"${CLOUDRON_APP_ORIGIN}/public/static/logo.png\"
else
    source /app/data/env.sh
    /app/code/listmonk --yes --idempotent --upgrade --config /run/listmonk/config.toml
fi

echo "==> Updating settings"
update_setting_json app.root_url \"${CLOUDRON_APP_ORIGIN}\"

return_path_header=""
if [[ -n "${CLOUDRON_MAIL_IMAP_SERVER:-}" ]]; then
    echo "==> Enabling bounce processing"
    update_setting_json bounce.enabled true
    # the return_path below is not used afaict
    bounce=$(cat <<-EOF
    [{"host": "${CLOUDRON_MAIL_IMAP_SERVER}", "port": ${CLOUDRON_MAIL_POP3_PORT}, "type": "pop", "uuid": "c540f9aa-90eb-43f5-b4b3-faa2527ed887", "enabled": true, "password": "${CLOUDRON_MAIL_IMAP_PASSWORD}", "username": "${CLOUDRON_MAIL_IMAP_USERNAME}", "return_path": "${CLOUDRON_MAIL_IMAP_USERNAME}", "tls_enabled": false, "auth_protocol": "userpass", "scan_interval": "3m", "tls_skip_verify": true}]
EOF
)
    update_setting_json bounce.mailboxes "${bounce}"
    return_path_header="{\"Return-Path\": \"${CLOUDRON_MAIL_IMAP_USERNAME}\"}"
fi

if [[ -n "${CLOUDRON_MAIL_SMTP_SERVER:-}" ]]; then
    echo "==> Configuring to use Cloudron email"
    update_setting_json app.from_email "\"${CLOUDRON_MAIL_FROM_DISPLAY_NAME:-Listmonk} <${CLOUDRON_MAIL_FROM}>\""

    smtp=$(cat <<-EOF
    [{"host": "${CLOUDRON_MAIL_SMTP_SERVER}", "port": ${CLOUDRON_MAIL_SMTPS_PORT}, "enabled": true, "password": "${CLOUDRON_MAIL_SMTP_PASSWORD}", "tls_type": "TLS", "username": "${CLOUDRON_MAIL_SMTP_USERNAME}", "max_conns": 10, "idle_timeout": "15s", "wait_timeout": "5s", "auth_protocol": "plain", "email_headers": [${return_path_header}], "hello_hostname": "", "max_msg_retries": 2, "tls_skip_verify": true}]
EOF
)
    update_setting_json smtp "${smtp}"
fi

# for now no user creation via OIDC
# https://github.com/knadh/listmonk/issues/2119
if [[ -n "${CLOUDRON_OIDC_ISSUER:-}" ]]; then
    echo "==> Update OIDC config"
    # CLOUDRON_OIDC_PROVIDER_NAME is not supported
    # https://github.com/knadh/listmonk/issues/2211
    oidc=$(cat <<EOF
{"enabled": true, "client_id": "${CLOUDRON_OIDC_CLIENT_ID}", "provider_url": "${CLOUDRON_OIDC_ISSUER}", "client_secret": "${CLOUDRON_OIDC_CLIENT_SECRET}"}
EOF
    )
    update_setting_json "security.oidc" "${oidc}"
fi

chown -R cloudron:cloudron /app/data /run/listmonk

echo "==> Starting listmonk"
exec gosu cloudron:cloudron /app/code/listmonk --config /run/listmonk/config.toml --static-dir=/app/data/static --i18n-dir=/app/data/i18n

