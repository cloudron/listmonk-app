This app is pre-setup with an admin account. The initial credentials are:

**Username**: admin<br/>
**Password**: changeme<br/>
**Email**: admin@cloudron.local<br/>

<sso>
Admin should create appropriate user accounts to let Cloudron users to login via SSO. The users are matched by Email address, **not** by username.
To avoid future user migration issues, it is still recommended to also set the same usernames as they have on Cloudron.
</sso>
