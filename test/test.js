#!/usr/bin/env node

/* jslint node:true */
/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, Key, until, Select } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.EMAIL || !process.env.PASSWORD) {
    console.log('USERNAME, EMAIL and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const TEST_TIMEOUT = 10000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const ADMIN_USERNAME = 'admin';
    const ADMIN_PASSWORD = 'changeme';

    const username = process.env.USERNAME;
    const email = process.env.EMAIL;
    const password = process.env.PASSWORD;
    const user_role = 1; // Super-Admin

    let browser, app;
    let athenticated_by_oidc = false;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    async function exists(selector) {
        await browser.wait(until.elementLocated(selector), TEST_TIMEOUT);
    }

    async function visible(selector) {
        await exists(selector);
        await browser.wait(until.elementIsVisible(browser.findElement(selector)), TEST_TIMEOUT);
    }

    async function login(username, password) {
        await browser.get('about:blank');
        await browser.sleep(3000);
        await browser.get(`https://${app.fqdn}/admin/login?next=%2Fadmin`);
        await browser.findElement(By.id('username')).sendKeys(username);
        await browser.findElement(By.id('password')).sendKeys(password);
        await browser.findElement(By.xpath('//button[text()="Login"]')).click();
        await visible(By.xpath('//span[contains(text(), "Dashboard")]'));
    }

    async function loginOIDC(username, password) {
        await browser.get(`https://${app.fqdn}`);
        await browser.sleep(2000);

        await visible(By.xpath('//a[@class="button"]'));
        await browser.findElement(By.xpath('//a[@class="button"]')).click();
        await browser.sleep(2000);

        await visible(By.xpath('//button[contains(@class, "button-outline")]'));
        await browser.findElement(By.xpath('//button[contains(@class, "button-outline")]')).click();
        await browser.sleep(2000);

        if (!athenticated_by_oidc) {
            await visible(By.id('inputUsername'));
            await browser.findElement(By.id('inputUsername')).sendKeys(username);
            await browser.findElement(By.id('inputPassword')).sendKeys(password);
            await browser.findElement(By.id('loginSubmitButton')).click();

            athenticated_by_oidc = true;
            await browser.sleep(2000);
        }

        await browser.sleep(2000);
        await visible(By.xpath('//span[contains(text(), "Dashboard")]'));
    }

    async function logout() {
        await browser.get(`https://${app.fqdn}/admin/settings`);
        await browser.sleep(2000);
        await visible(By.xpath('//div[@class="navbar-link"]'));
        await browser.findElement(By.xpath('//div[@class="navbar-link"]')).click();
        await browser.sleep(2000);
        await browser.findElement(By.xpath('//a[contains(text(), "Logout")]')).click();
        await browser.sleep(2000);
        await visible(By.xpath('//a[contains(text(), "Login")]'));
        // await browser.findElement(By.xpath('//a[contains(text(), "Login")]')).click();
    }

    async function createOIDCUser(username, email, role) {
        await browser.get('https://' + app.fqdn + '/admin/users');
        await browser.sleep(2000);
        await visible(By.xpath('//button[contains(@class, "btn-new")]'));
        await browser.findElement(By.xpath('//button[contains(@class, "btn-new")]')).click();
        await browser.sleep(2000);
        await visible(By.xpath('//input[@name="username"]'));
        await browser.findElement(By.xpath('//input[@name="username"]')).sendKeys(username);
        await browser.findElement(By.xpath('//input[@name="email"]')).sendKeys(email);
        await browser.sleep(1000);

        let roleSelectElement = await browser.findElement(By.xpath('//select[@name="user_role"]'));
        await browser.actions({async: true}).move({origin: roleSelectElement});//.click().perform();
        await browser.executeScript('arguments[0].scrollIntoView(true)', roleSelectElement);
        const select = new Select(roleSelectElement)
        select.selectByValue('1');
        await browser.sleep(2000);
        await browser.findElement(By.xpath('//button[@data-cy="btn-save"]')).click();
        await browser.sleep(2000);
        await visible(By.xpath('//td[@data-label="Username"]'));
    }

    async function changeLogo() {
        await browser.get('https://' + app.fqdn + '/admin/settings');
        await visible(By.xpath('//input[@name="app.logo_url"]'));
        await browser.sleep(3000);
        await browser.findElement(By.xpath('//input[@name="app.logo_url"]')).sendKeys(Key.chord(Key.CONTROL, 'a') + Key.chord(Key.COMMAND, 'a') + Key.BACK_SPACE);
        await browser.sleep(3000);
        await browser.findElement(By.xpath('//input[@name="app.logo_url"]')).sendKeys('https://www.cloudron.io/img/logo64.png');
        await browser.sleep(3000);
        await browser.findElement(By.xpath('//button[contains(@class, "isSaveEnabled")]')).click();
        await browser.sleep(4000);
    }

    async function checkLogo() {
        await browser.get('https://' + app.fqdn);
        await visible(By.xpath('//img[@src="https://www.cloudron.io/img/logo64.png"]'));
    }

    function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION || a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app no sso', function () { execSync('cloudron install --no-sso --location ' + LOCATION, EXEC_ARGS); });

    it('can get app information', getAppInfo);

    it('can login', login.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));
    it('can change logo', changeLogo);
    it('can logout', logout);
    it('can check logo', checkLogo);

    it('uninstall app', function () { execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS); });

    it('install app sso', function () { execSync('cloudron install --location ' + LOCATION, EXEC_ARGS); });

    it('can get app information', getAppInfo);

    it('can login', login.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));
    it('can change logo', changeLogo)
    it('can create OIDC user', createOIDCUser.bind(null, username, email, user_role));
    it('can logout', logout);

    it('can check logo', checkLogo);

    // login via OIDC needs a restart
    it('restart app', function () { execSync('cloudron restart --app ' + app.id, EXEC_ARGS); });

    it('can check logo', checkLogo);
    it('can login via OIDC', loginOIDC.bind(null, username, password));
    it('can logout', logout);

    it('backup app', function () { execSync('cloudron backup create --app ' + app.id, EXEC_ARGS); });

    it('restore app', function () {
        const backups = JSON.parse(execSync('cloudron backup list --raw --app ' + app.id));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can check logo', checkLogo);
    it('can login via OIDC', loginOIDC.bind(null, username, password));
    it('can logout', logout);

    it('move to different location', function () {
        browser.manage().deleteAllCookies();
        execSync('cloudron configure --location ' + LOCATION + '2 --app ' + app.id, EXEC_ARGS);
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
    });

    it('can login', login.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));
    it('can logout', logout);
    it('can check logo', checkLogo);

    it('can login via OIDC', loginOIDC.bind(null, username, password));
    it('can logout', logout);

    it('uninstall app', function () { execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS); });

    // test update
    it('can install app for update', function () { execSync(`cloudron install --appstore-id ${app.manifest.id} --location ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can login', login.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));
    it('can change logo', changeLogo);
    it('can logout', logout);
    it('can check logo', checkLogo);

    it('can update', function () {
        execSync('cloudron update --app ' + app.id, EXEC_ARGS);
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];
        expect(app).to.be.an('object');
    });

    it('can login', login.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));
    it('can logout');
    it('can check logo', checkLogo);

    it('uninstall app', function () { execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS); });
});
