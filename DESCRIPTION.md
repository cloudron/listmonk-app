## About

Listmonk is a standalone, self-hosted, newsletter and mailing list manager. It is fast, feature-rich, and packed into a single binary.

## Features

* Mailing lists - Manage millions of subscribers across many single and double opt-in lists with custom JSON attributes for each subscriber. Query and segment subscribers with SQL expressions.
* Analytics - - Create powerful, dynamic e-mail templates with the Go templating languageVisualize and compare campaign performance. Connect external visualization programs to the database easily with the simple table structure.
* Templating - Create powerful, dynamic e-mail templates with the Go templating language
* Performance - Highly configurable, multi-threaded, high-throughput multi-SMTP e-mail queues for super fast campaign delivery
* Media - More than just e-mail campaigns. Connect messenger web services and send SMS, Whatsapp, FCM notifications, or any type of arbitrary messages with simple webhooks.Use the media manager to upload images for e-mail campaigns on the server's filesystem, Amazon S3, or any S3 compatible (Minio) backend.
* Messengers - More than just e-mail campaigns. Connect messenger web services and send SMS, Whatsapp, FCM notifications, or any type of arbitrary messages with simple webhooks.
* Privacy - Allow subscribers to permanantely blocklist themselves, export all the data associated with their profile including clicks and views, and to wipe all their data in a single click.

